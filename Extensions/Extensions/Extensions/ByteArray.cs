﻿using System;
using System.Collections.Generic;

namespace System
{
    namespace Extensions
    {
        public static class ByteArray
        {
            #region "Perform an indexed search for a pattern within another char array"
            private static Dictionary<byte, List<int>> index = null;

            public static IEnumerable<int> Search_Indexed(this byte[] source, byte[] pattern, bool recreateindex = false, bool findoverlapping = false)
            {
                // Do we have a pattern or is its length == 0?
                if (pattern == null || pattern.Length == 0)
                    yield break;

                // Do we have an index or are we forced to rebuild it?
                if (index == null || recreateindex)
                {
                    // Clear the index
                    if (index != null)
                        index.Clear();

                    // Create a new index and fill it
                    index = new Dictionary<byte, List<int>>();

                    for (byte i = 0; i <= 255; i++)
                        index.Add(i, new List<int>());

                    for (int i = 0; i < source.Length; i++)
                        index[source[i]].Add(i);
                }

                int overlap = -1;

                
                if (pattern.Length == 1)
                    // With a pattern length of 1 we just returns the positions for that value
                    foreach (int i in index[pattern[0]])
                        yield return i;
                else
                    // Find each startingposition of the patterns first byte
                    foreach (int i in index[pattern[0]])
                    {
                        // If the pattern is longer than we have left to examine then we break early
                        if (i + pattern.Length > source.Length)
                            yield break;

                        // No overlap-search is enabled
                        if (overlap > i)
                            continue;

                        bool found = true;
                        // Search for the pattern
                        for (int j = 1; j < pattern.Length && found; j++)
                            if (pattern[j] != source[i + j])
                                found = false;

                        // Pattern found?
                        if (found)
                            // Yes, send it out
                            yield return i;

                        // Are we searching overlapped?
                        if (!findoverlapping)
                            // Nope, prevent any hits that could occur within the found pattern
                            overlap = i + pattern.Length;
                    }
            }
            #endregion
        }
    }
}