﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Extensions
{
    public class Palette
    {
        private Dictionary<int, int> lookup_table = new Dictionary<int, int>();
        private List<LAB> palette_lab = new List<LAB>();

        public void AddRange(System.Drawing.Color[] colors)
        {
            foreach (System.Drawing.Color color in colors)
                Add(color);
        }

        public void AddRange(int[] colors)
        {
            foreach (int color in colors)
                Add(color);
        }

        public void Add(System.Drawing.Color color)
        {
            palette_lab.Add(new LAB(color));
        }

        public void Add(int color)
        {
            color = (int)(color | 0xff000000);
            palette_lab.Add(new LAB(System.Drawing.Color.FromArgb(color)));
        }

        public System.Drawing.Color GetNearestColor(System.Drawing.Color color)
        {
            return GetNearestColor(color.ToArgb());
        }

        public System.Drawing.Color GetNearestColor(int color)
        {
            int requested_color = color;
            int resulting_color = 0;
            if (lookup_table.TryGetValue(requested_color, out resulting_color))
                return System.Drawing.Color.FromArgb(resulting_color);

            LAB lab = new LAB(System.Drawing.Color.FromArgb(requested_color));
            System.Drawing.Color best_color = (from q in palette_lab orderby q.EuclideanDistance(lab) ascending select q).First().RGB();
            lookup_table.Add(requested_color, best_color.ToArgb());
            return best_color;
        }

        public void Clear()
        {
            lookup_table.Clear();
        }
    }

    public static class Color
    {
        public static System.Drawing.Color Average(this System.Drawing.Color color, System.Drawing.Color  other)
        {
            LAB col1 = new LAB(color);
            LAB col2 = new LAB(other);
            LAB avg = new LAB
            {
                L = (col1.L + col2.L) / 2.0,
                A = (col1.A + col2.A) / 2.0,
                B = (col1.B + col2.B) / 2.0
            };
            return avg.RGB();
        }
    }

    public struct XYZ
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        private static double FROM_RGB(double v)
        {
            if (v > 0.04045)
                return Math.Pow(((v + 0.055) / 1.055), 2.4);
            else
                return v / 12.92;
        }

        private static double FROM_LAB(double v)
        {
            if (Math.Pow(v, 3) > 0.008856)
                return Math.Pow(v, 3);
            else
                return (v - 16.0 / 116.0) / 7.787;
        }

        public XYZ (int color)
        {
            double r = FROM_RGB(((color >> 16) & 255) / 255.0) * 100.0;
            double g = FROM_RGB(((color >> 8) & 255) / 255.0) * 100.0;
            double b = FROM_RGB((color & 255) / 255.0) * 100.0;

            X = r * 0.4124 + g * 0.3576 + b * 0.1805;
            Y = r * 0.2126 + g * 0.7152 + b * 0.0722;
            Z = r * 0.0193 + g * 0.1192 + b * 0.9505;
        }

        public XYZ(System.Drawing.Color color)
        {
            double r = FROM_RGB(color.R / 255.0) * 100.0;
            double g = FROM_RGB(color.G / 255.0) * 100.0;
            double b = FROM_RGB(color.B / 255.0) * 100.0;

            X = r * 0.4124 + g * 0.3576 + b * 0.1805;
            Y = r * 0.2126 + g * 0.7152 + b * 0.0722;
            Z = r * 0.0193 + g * 0.1192 + b * 0.9505;
        }

        public XYZ(LAB color)
        {
            Y = FROM_LAB((color.L + 16.0) / 116.0);
            X = FROM_LAB(color.A / 500.0 + Y);
            Z = FROM_LAB(Y - color.B / 200.0);

            X = X * 94.811;
            Y = Y * 100.000;
            Z = Z * 107.304;
        }

        private static double RGB(double v)
        {
            if (v > 0.0031308)
                return 1.055 * (Math.Pow(v, (1.0 / 2.4))) - 0.055;
            else
                return 12.92 * v;
        }
        public System.Drawing.Color RGB()
        {
            double x = X / 100.0;
            double y = Y / 100.0;
            double z = Z / 100.0;

            double r = x * 3.2406 + y * -1.5372 + z * -0.4986;
            double g = x * -0.9689 + y * 1.8758 + z * 0.0415;
            double b = x * 0.0557 + y * -0.2040 + z * 1.0570;

            r = RGB(r);
            g = RGB(g);
            b = RGB(b);

            return System.Drawing.Color.FromArgb(255, (int)(r * 255.0), (int)(g * 255.0), (int)(b * 255.0));
        }
    }

    public struct LAB
    {
        public double L { get; set; }
        public double A { get; set; }
        public double B { get; set; }

        private static double XYZ(double v)
        {
            if (v > 0.008856)
                return Math.Pow(v, (1.0 / 3.0));
            else
                return (7.787 * v) + (16.0 / 116.0);
        }

        public LAB(XYZ color)
        {
            double x = XYZ(color.X / 94.811);
            double y = XYZ(color.Y / 100.000);
            double z = XYZ(color.Z / 107.304);
            L = (116.0 * y) - 16.0;
            A = 500.0 * (x - y);
            B = 200.0 * (y - z);
        }

        public LAB(int color)
        {
            XYZ xyz = new XYZ(color);
            LAB lab = new LAB(xyz);
            L = lab.L;
            A = lab.A;
            B = lab.B;
        }

        public LAB(System.Drawing.Color color)
        {
            XYZ xyz = new XYZ(color);
            LAB lab = new LAB(xyz);
            L = lab.L;
            A = lab.A;
            B = lab.B;
        }

        public System.Drawing.Color RGB()
        {
            XYZ xyz = new XYZ(this);
            return xyz.RGB();
        }

        public double EuclideanDistance(LAB other)
        {
            double Ld = L - other.L;
            double Ad = A - other.A;
            double Bd = B - other.B;
            return Math.Sqrt(Ld * Ld + Ad * Ad + Bd * Bd);
        }
    }
}

